﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;


/// <summary>
/// Промокоды
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PromoCodesController
    : ControllerBase
{
    private readonly IRepository<PromoCode> _repository;

    public PromoCodesController(IRepository<PromoCode> repository)
    {
        _repository = repository ?? throw new ArgumentNullException(nameof(repository));
    }

    /// <summary>
    /// Получить все промокоды
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
    {
        var promoCodes = await _repository.GetAllAsync();
        return promoCodes
            .Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToLongDateString(),
                EndDate = x.EndDate.ToLongDateString(),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo,
            })
            .ToList();
    }
        
    /// <summary>
    /// Создать промокод и выдать его клиентам с указанным предпочтением
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
    {
        //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
        throw new NotImplementedException();
    }
}