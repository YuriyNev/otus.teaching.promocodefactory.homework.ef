﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Список предпочтений
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PreferenceController : ControllerBase
{
    private readonly IRepository<Preference> _repository;

    public PreferenceController(IRepository<Preference> repository)
    {
        _repository = repository ?? throw new ArgumentNullException(nameof(repository));
    }

    /// <summary>
    /// Получить список предпочтений
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<IReadOnlyList<PreferenceResponse>> GetPreferences()
    {
        var preferences = await _repository.GetAllAsync();

        return preferences
            .Select(x => new PreferenceResponse { Id = x.Id, Name = x.Name })
            .ToList();
    }
}