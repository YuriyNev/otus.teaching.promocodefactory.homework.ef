﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public class CustomerResponse : CustomerEntity
{
    public Guid Id { get; set; }
    public List<PreferenceShortResponse> Preferences { get; set; }
    public List<PromoCodeShortResponse> PromoCodes { get; set; }
}

public class PreferenceShortResponse
{
    public string Preference { get; set; }
}

public class PreferenceResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}