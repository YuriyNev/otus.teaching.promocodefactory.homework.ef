﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
        
        // говорит о том, что одна роль может использоваться у разных сотрудников 
        public ICollection<Employee> Employees { get; set; }
    }
}