﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data;

public static class FakeDataFactory
{
    public static IEnumerable<Employee> Employees => new List<Employee>()
    {
        new()
        {
            Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
            Email = "owner@somemail.ru",
            FirstName = "Иван",
            LastName = "Сергеев",
            RoleId = Roles.FirstOrDefault(x => x.Name == "Admin")?.Id,
            // Role - удалил. Верно ли? Иначе данные не заполнялись. 
            AppliedPromocodesCount = 5
        },
        new()
        {
            Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
            Email = "andreev@somemail.ru",
            FirstName = "Петр",
            LastName = "Андреев",
            RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager")?.Id,
            AppliedPromocodesCount = 10
        },
        new()
        {
            Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435846"),
            Email = "andreev1@somemail.ru",
            FirstName = "Иван",
            LastName = "Николаев",
            RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager")?.Id,
            AppliedPromocodesCount = 10
        },
    };

    public static IEnumerable<Role> Roles => new List<Role>()
    {
        new Role()
        {
            Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
            Name = "Admin",
            Description = "Администратор",
        },
        new Role()
        {
            Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
            Name = "PartnerManager",
            Description = "Партнерский менеджер"
        }
    };

    public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
    {
        new()
        {
            Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7626"),
            //Preference = Preferences.First(),
            PreferenceId = Preferences.FirstOrDefault()?.Id,
            Code = "PROMO20",
            BeginDate = DateTime.Today.AddDays(-10),
            EndDate = DateTime.Today.AddDays(10),
            //PartnerManager = Employees.Skip(1).First(),
            PartnerManagerId = Employees.Skip(1).FirstOrDefault()?.Id,
            PartnerName = "PEPSICO",
            CustomerId = Customers.Skip(0).FirstOrDefault()?.Id, 
        },
        new()
        {
            Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e4625"),
            //Preference = Preferences.First(),
            PreferenceId = Preferences.Skip(1).FirstOrDefault()?.Id,
            Code = "SUPERPROMO",
            BeginDate = DateTime.Today.AddDays(-10),
            EndDate = DateTime.Today.AddDays(10),
            //PartnerManager = Employees.Skip(1).First(),
            PartnerManagerId = Employees.Skip(2).FirstOrDefault()?.Id,
            PartnerName = "ТеатрТеатр",
            CustomerId = Customers.Skip(1).FirstOrDefault()?.Id, 
        },
    };

    public static IEnumerable<CustomerPreference> CustomerPreferences => new List<CustomerPreference>()
    {
        new()
        {
            Id = Guid.Parse("f0cefd27-b859-49c2-930d-19e8e1739589"),
            CustomerId = Customers.FirstOrDefault()?.Id,
            PreferenceId = Preferences.FirstOrDefault()?.Id,
        },new()
        {
            Id = Guid.Parse("f0cefd27-b859-49c2-930d-19e8e1739589"),
            CustomerId = Customers.Skip(1).FirstOrDefault()?.Id,
            PreferenceId = Preferences.Skip(1).FirstOrDefault()?.Id,
        },
    };

    public static IEnumerable<Preference> Preferences => new List<Preference>()
    {
        new()
        {
            Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
            Name = "Театр",
        },
        new()
        {
            Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
            Name = "Семья",
        },
        new()
        {
            Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
            Name = "Дети",
        }
    };

    public static IEnumerable<Customer> Customers => new List<Customer>()
    {
        new()
        {
            Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
            Email = "ivan_sergeev@mail.ru",
            FirstName = "Иван",
            LastName = "Петров",
        },
        new()
        {
            Id = Guid.Parse("246cd6b0-d92f-49ef-bd48-86a570f36671"),
            Email = "ultra_dimasik1999@hotmail.com",
            FirstName = "Дмитрий",
            LastName = "Галиев",
        },
    };
}