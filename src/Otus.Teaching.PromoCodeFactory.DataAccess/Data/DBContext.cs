using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data;

public sealed class PromoDbContext : DbContext
{
    public DbSet<PromoCode> PromoCodes { get; set; }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Preference> Preferences { get; set; }
    public DbSet<Employee> Employees { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<CustomerPreference> CustomerPreferences { get; set; }

    public PromoDbContext(DbContextOptions<PromoDbContext> options) : base(options)
    {
        var deleted = Database.EnsureDeleted();
        var created = Database.EnsureCreated();
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        
        modelBuilder
            .Entity<Employee>()
            .HasOne(employee => employee.Role)
            .WithMany(role => role.Employees)
            .HasForeignKey(employee => employee.RoleId);

        modelBuilder
            .Entity<PromoCode>()
            .HasOne(promoCode => promoCode.PartnerManager)
            .WithMany(employee => employee.PromoCodes)
            .HasForeignKey(promoCode => promoCode.PartnerManagerId);
        
        modelBuilder
            .Entity<PromoCode>()
            .HasOne(promoCode => promoCode.Preference)
            .WithMany(preference => preference.PromoCodes)
            .HasForeignKey(promoCode => promoCode.PreferenceId);
        
        modelBuilder
            .Entity<PromoCode>()
            .HasOne(promoCode => promoCode.Customer)
            .WithMany(preference => preference.PromoCodes)
            .HasForeignKey(promoCode => promoCode.CustomerId);
        
        modelBuilder.Entity<CustomerPreference>()
            .HasKey(x => new { x.CustomerId, x.PreferenceId });

        modelBuilder.Entity<CustomerPreference>()
            .HasOne(x => x.Customer)
            .WithMany(x => x.CustomerPreferences)
            .HasForeignKey(x => x.CustomerId);

        modelBuilder.Entity<CustomerPreference>()
            .HasOne(x => x.Preference)
            .WithMany(x => x.CustomerPreferences)
            .HasForeignKey(x => x.PreferenceId);
        
        
        modelBuilder.Entity<Employee>().Property(p => p.FirstName).HasMaxLength(50);
        modelBuilder.Entity<Employee>().Property(p => p.LastName).HasMaxLength(50);
        modelBuilder.Entity<Employee>().Property(p => p.Email).HasMaxLength(50);
        modelBuilder.Entity<PromoCode>().Property(p => p.Code).HasMaxLength(50);
        modelBuilder.Entity<PromoCode>().Property(p => p.PartnerName).HasMaxLength(50);
        
        // Data seeding
        modelBuilder
            .Entity<Employee>()
            .HasData(FakeDataFactory.Employees);
        modelBuilder
            .Entity<Role>().HasData(FakeDataFactory.Roles);
        modelBuilder
            .Entity<Preference>()
            .HasData(FakeDataFactory.Preferences);
           
        modelBuilder
            .Entity<Customer>()
            .HasData(FakeDataFactory.Customers);
        
        modelBuilder
            .Entity<CustomerPreference>()
            .HasData(FakeDataFactory.CustomerPreferences);
        
        modelBuilder
            .Entity<PromoCode>()
            .HasData(FakeDataFactory.PromoCodes);
    }
    
    public override void Dispose()
    {
        var deleted = Database.EnsureDeleted();
        ChangeTracker.Clear();
        base.Dispose();
    }
}