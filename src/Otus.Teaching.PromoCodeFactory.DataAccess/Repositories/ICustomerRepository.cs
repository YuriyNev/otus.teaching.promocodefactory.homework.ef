﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public interface ICustomerRepository : IRepository<Customer>
{
    Task CreateAsync(Customer customer, IReadOnlyList<Guid> preferences);
    Task ModifyAsync(Customer customer, IReadOnlyList<Guid> preferences);
    Task DeleteAsync(Guid id);
}