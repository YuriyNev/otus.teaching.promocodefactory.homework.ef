using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EmployeeRepository : EfRepository<Employee>
{
    public EmployeeRepository(PromoDbContext context) : base(context) { }

    public override async Task<Employee> GetByIdAsync(Guid id)
    {
        var employee = await DbContext.Employees
            .Include(x => x.Role)
            .FirstOrDefaultAsync(x => x.Id == id);
        return employee;
    }
}

public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
{
    public CustomerRepository(PromoDbContext context) : base(context) { }

    public override async Task<Customer> GetByIdAsync(Guid id) =>
        await DbContext.Customers
            .Where(x => x.Id == id)
            .Include(x => x.PromoCodes)
            .Include(x => x.CustomerPreferences)
            .ThenInclude(x => x.Preference)
            .FirstAsync();

    public async Task CreateAsync(Customer customer, IReadOnlyList<Guid> preferences)
    {
        DbContext.Customers.Add(customer);
        await DbContext.CustomerPreferences
            .AddRangeAsync(preferences
                .Select(x => new CustomerPreference
                {
                    Id = Guid.NewGuid(),
                    CustomerId = customer.Id,
                    PreferenceId = x,
                }));

        await DbContext.SaveChangesAsync();
    }

    public async Task ModifyAsync(Customer customer, IReadOnlyList<Guid> preferences)
    {
        var modifyCustomer = await DbContext.Customers
            .FirstOrDefaultAsync(x => x.Id == customer.Id);

        if (modifyCustomer == null)
            throw new ArgumentException(nameof(Customer.Id));

        modifyCustomer.FirstName = customer.FirstName;
        modifyCustomer.LastName = customer.LastName;
        modifyCustomer.Email = customer.Email;
        await DbContext.CustomerPreferences
            .AddRangeAsync(
                preferences.Select(preferenceId => new CustomerPreference
                {
                    Id = Guid.NewGuid(),
                    CustomerId = modifyCustomer.Id,
                    PreferenceId = preferenceId,
                }));
        await DbContext.SaveChangesAsync();
    }

    public override async Task DeleteAsync(Guid id)
    {
        var removeItem = await DbContext.Customers
            .FirstAsync(x => x.Id == id);
        
        var promoCode = await DbContext.PromoCodes.FirstOrDefaultAsync(x => x.CustomerId == id);
        
        DbContext.Remove(promoCode);
        DbContext.Customers.Remove(removeItem);
        await DbContext.SaveChangesAsync();
    }
}

public class PromoCodeRepository : EfRepository<PromoCode>
{
    public PromoCodeRepository(PromoDbContext context) : base(context) { }
}

public class RoleRepository : EfRepository<Role>
{
    public RoleRepository(PromoDbContext context) : base(context) { }
}

public class PreferenceRepository : EfRepository<Preference>
{
    public PreferenceRepository(PromoDbContext context) : base(context) { }
}

public class EfRepository<T> : IRepository<T>, IDisposable
    where T : BaseEntity
{
    protected readonly PromoDbContext DbContext;

    private DbSet<T> DbSet => DbContext.Set<T>();

    protected EfRepository(PromoDbContext promoDbContext)
    {
        DbContext = promoDbContext ?? throw new ArgumentNullException(nameof(promoDbContext));
    }

    public async Task<IEnumerable<T>> GetAllAsync() 
        => await DbSet.ToListAsync();

    public virtual async Task<T> GetByIdAsync(Guid id) 
        => await DbSet.FirstOrDefaultAsync(x => x.Id == id);
    
    public virtual async Task DeleteAsync(Guid id)
    {
        var element = await DbSet.FirstOrDefaultAsync(x => x.Id == id);
        
        DbSet.Remove(element);
        await DbContext.SaveChangesAsync();
    }

    public void Dispose()
    {
    }
}